package com.example.demo.Manager.impl;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.EmpleadoEntity;
import com.example.demo.Manager.EmpleadoManager;
import com.example.demo.Repository.EmpleadoRepository;
import com.example.demo.Util.Constantes;
import com.example.demo.Util.Mensajes;

@Service
public class EmpleadoManagerImpl implements EmpleadoManager {

	@Autowired
	private EmpleadoRepository empleadoRepository;

	private Constantes constantes;

	private Mensajes mensajes;

	private static Logger logJava = Logger.getLogger(EmpleadoManagerImpl.class);

	@Override
	public List<EmpleadoEntity> consultaTodosEmpleados() throws SQLException {
		return empleadoRepository.obtenerTodosEmpleados();
	}

	@SuppressWarnings("static-access")
	@Override
	@Transactional
	public void registrarEmpleado(EmpleadoEntity empleado) throws SQLException {

		empleado.setTipoId(tipoIdentificacion(empleado.getTipoId()));

		if (esRegistroValido(empleado)) {

			EmpleadoEntity empleadoFinal;
			String pattern = constantes.FORMATO_FECHA;
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			empleado.setFechaEdicion(constantes.INICIALIZAR_FECHA);
			empleado.setFechaHoraRegistro(simpleDateFormat.format(new Date()));
			empleado.setEstado(constantes.ACTIVO);

			empleadoFinal = empleadoRepository.registrarEmpleado(empleado);
			empleadoFinal.setEmail(generarCorreo(empleado));
			empleadoFinal.setPaisEmpleo(tipoPais(empleadoFinal.getPaisEmpleo()));

			empleadoFinal.setArea(tipoArea(empleadoFinal.getArea()));

			empleadoRepository.actualizarEmpleado(empleadoFinal);
		}
	}

	/*
	 * metodo para validar variables vacias
	 */
	public boolean esVacio(String text) {
		return text == null || text == "";
	}

	/*
	 * metodo para realizar la conversion del valor al nombre para el area
	 */
	@SuppressWarnings("static-access")
	private String tipoArea(String area) {
		if (Integer.parseInt(area) == constantes.ADMINISTRACION) {
			area = mensajes.ADMINISTRACION;
		} else if (Integer.parseInt(area) == (constantes.FINANCIERA)) {
			area = mensajes.FINANCIERA;
		} else if (Integer.parseInt(area) == (constantes.COMPRAS)) {
			area = mensajes.COMPRAS;
		} else if (Integer.parseInt(area) == (constantes.INFRAESCTRUCTURA)) {
			area = mensajes.INFRAESTRUCTURA;
		} else if (Integer.parseInt(area) == (constantes.OPERACION)) {
			area = mensajes.OPERACION;
		} else if (Integer.parseInt(area) == (constantes.TALENTO_HUMANO)) {
			area = mensajes.TALENTO;
		} else if (Integer.parseInt(area) == (constantes.SERVICIO_VARIOS)) {
			area = mensajes.SERVICIOS_VARIOS;
		}

		return area;
	}

	/*
	 * metodo para realizar la conversion del valor al nombre para el pais
	 */
	@SuppressWarnings("static-access")
	private String tipoPais(String pais) {
		if (Integer.parseInt(pais) == (constantes.COLOMBIA)) {
			pais = mensajes.COLOMBIA;
		} else if (Integer.parseInt(pais) == (constantes.USA)) {
			pais = mensajes.USA;
		}
		return pais;
	}

	/*
	 * metodo para realizar la conversion del valor al nombre para el areatipo de
	 * documento
	 */
	@SuppressWarnings("static-access")
	private String tipoIdentificacion(String tipoId) {
		if (Integer.parseInt(tipoId) == (constantes.CEDULA_CIUDADANIA)) {
			tipoId = mensajes.CC;
		} else if (Integer.parseInt(tipoId) == (constantes.CEDULA_EXTRANJERA)) {
			tipoId = mensajes.CE;
		} else if (Integer.parseInt(tipoId) == (constantes.PASAPORTE)) {
			tipoId = mensajes.PASAPORTE;
		} else if (Integer.parseInt(tipoId) == (constantes.PERMISO_ESP)) {
			tipoId = mensajes.PERMISO;
		}

		return tipoId;
	}

	/*
	 * validaciones de campos para inserccion
	 */
	@SuppressWarnings("static-access")
	private boolean esRegistroValido(EmpleadoEntity empleado) throws SQLException {

		boolean registroValido = true;
		Pattern mayusculas = Pattern.compile("[A-Z]+");
		Pattern identificacion = Pattern.compile("^[a-zA-Z0-9-]*$");

		if (!mayusculas.matcher(empleado.getpNombre()).matches() || esVacio(empleado.getpNombre())
				|| empleado.getpNombre().length() > 20) {
			registroValido = false;
			logJava.error(mensajes.ERROR_PRIMER_NOMBRE);
		}
		if (!mayusculas.matcher(empleado.getpApellido()).matches() || esVacio(empleado.getpApellido())
				|| empleado.getpApellido().length() > 20) {
			registroValido = false;
			logJava.error(mensajes.ERROR_PRIMER_APELLIDO);
		}
		if (!mayusculas.matcher(empleado.getsApellido()).matches() || esVacio(empleado.getsApellido())
				|| empleado.getsApellido().length() > 20) {
			registroValido = false;
			logJava.error(mensajes.ERROR_SEGUNDO_APELLIDO);
		}
		if (!mayusculas.matcher(empleado.getoNombres()).matches() || esVacio(empleado.getoNombres())
				|| empleado.getoNombres().length() > 50) {
			registroValido = false;
			logJava.error(mensajes.ERROR_OTROS_NOMBRES);
		}
		if (!identificacion.matcher(empleado.getNumeroId()).matches() || esVacio(empleado.getNumeroId())
				|| empleado.getNumeroId().length() > 20) {
			registroValido = false;
			logJava.error(mensajes.ERROR_NRO_ID);
		}
		if (empleadoRepository.existeTipoyNumeroId(empleado.getTipoId(), empleado.getNumeroId(), empleado.getIdEmpleado())) {
			registroValido = false;
			logJava.error(mensajes.DOBLE_ID);
		}

		return registroValido;
	}

	@Override
	@Transactional
	public void eliminarEmpleado(Long idEmpleado) throws SQLException {
		empleadoRepository.eliminarEmpleado(idEmpleado);
	}

	@Override
	public EmpleadoEntity consultaEmpleadoById(Long idEmpleado) throws SQLException {
		return empleadoRepository.consultaEmpleadoById(idEmpleado);
	}

	/*
	 * metodo para generacion del correo
	 */
	@SuppressWarnings("static-access")
	private String generarCorreo(EmpleadoEntity empleado) throws SQLException {

		StringBuilder correo = new StringBuilder();
		String correoFinal = "";

		if (Integer.parseInt(empleado.getPaisEmpleo()) == (constantes.COLOMBIA)) {
			correo = new StringBuilder(empleado.getpNombre()).append(constantes.PUNTO)
					.append(empleado.getpApellido().replaceAll("\\s", "")).append(constantes.ARROBA)
					.append(constantes.DOMINIO_COLOMBIA);
		} else if (Integer.parseInt(empleado.getPaisEmpleo()) == (constantes.USA)) {
			correo = new StringBuilder(empleado.getpNombre()).append(constantes.PUNTO)
					.append(empleado.getpApellido().replaceAll("\\s", "")).append(constantes.ARROBA)
					.append(constantes.DOMINIO_USA);
		}

		correoFinal = new String(correo);

		if (empleadoRepository.existeCorreo(correoFinal)) {

			int at = correoFinal.indexOf(constantes.ARROBA);
			correoFinal = correoFinal.substring(0, at) + constantes.PUNTO + empleado.getIdEmpleado() + correoFinal.substring(at);

		}

		return correoFinal;
	}

	@SuppressWarnings("static-access")
	@Override
	@Transactional
	public void actualizarEmpleado(EmpleadoEntity empleado) throws SQLException {

		empleado.setTipoId(tipoIdentificacion(empleado.getTipoId()));

		if (esRegistroValido(empleado)) {

			String pattern = constantes.FORMATO_FECHA;
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

			empleado.setFechaEdicion(simpleDateFormat.format(new Date()));
			empleado.setEstado(constantes.ACTIVO);
			empleado.setEmail(generarCorreo(empleado));
			empleado.setPaisEmpleo(tipoPais(empleado.getPaisEmpleo()));
			empleado.setArea(tipoArea(empleado.getArea()));

			empleadoRepository.actualizarEmpleado(empleado);
		}
	}

}
