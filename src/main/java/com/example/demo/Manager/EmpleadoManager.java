package com.example.demo.Manager;

import java.sql.SQLException;
import java.util.List;

import com.example.demo.Entity.EmpleadoEntity;


public interface EmpleadoManager {

	/**
	 * permite consultar todos los empleados de la bd
	 * @return
	 * @throws SQLException
	 */
	List<EmpleadoEntity> consultaTodosEmpleados() throws SQLException;
	
	/**
	 * permite almacenar un registro de empleado
	 * @param empleado
	 * @throws SQLException
	 */
	void registrarEmpleado(EmpleadoEntity empleado) throws SQLException;
	
	/**
	 * permite eliminar un empleado por el id generado
	 * @param idEmpleado
	 * @throws SQLException
	 */
	void eliminarEmpleado(Long idEmpleado) throws SQLException;
	
	/**
	 * permmite consultar un empleado por la id generada
	 * @param idEmpleado
	 * @return
	 * @throws SQLException
	 */
	EmpleadoEntity consultaEmpleadoById(Long idEmpleado) throws SQLException;
	
	/**
	 * permite actualizar un empleado en la bd
	 * @param empleado
	 * @throws SQLException
	 */
	void actualizarEmpleado(EmpleadoEntity empleado) throws SQLException;
	
}
