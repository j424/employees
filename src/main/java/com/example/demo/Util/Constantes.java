package com.example.demo.Util;

public final class Constantes {

	
	public  Constantes() {
		
	}
	
	public static final Integer COLOMBIA = 1;
	public static final Integer USA = 2;
	
	public static final Integer CEDULA_CIUDADANIA = 1;
	public static final Integer CEDULA_EXTRANJERA = 2;
	public static final Integer PASAPORTE = 3;
	public static final Integer PERMISO_ESP = 4;

	public static final Integer ADMINISTRACION = 1;
	public static final Integer FINANCIERA = 2;
	public static final Integer COMPRAS = 3;
	public static final Integer INFRAESCTRUCTURA = 4;
	public static final Integer OPERACION = 5;
	public static final Integer TALENTO_HUMANO = 6;
	public static final Integer SERVICIO_VARIOS = 7;
	

	
	public static final String ARROBA = "@";
	public static final String DOMINIO_COLOMBIA = "cidenet.com.co";
	public static final String DOMINIO_USA = "cidenet.com.us";
	public static final String PUNTO = ".";
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String ACTIVO = "Activo";
	public static final String INICIALIZAR_FECHA = "01/01/1900";
	
	
	public static final String INFORMACION_CONEXION = "jdbc:oracle:thin:@localhost:1521:orcl";
	public static final String USUARIO = "system";
	public static final String CLAVE = "password";
	
}
