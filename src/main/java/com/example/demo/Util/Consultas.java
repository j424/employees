package com.example.demo.Util;

public class Consultas {

	
	public Consultas() {
		
	}
	
	public static final String DELETE_EMPLEADO = "delete from empleado where id_empleado = ? ";
	
	public static final String SELECT_TODO_EMPLEADO = 	"select * from empleado";
	
	public static final String SELECT_SECUENCIA_EMPLEADO = 	"SELECT empleado_seq.CURRVAL FROM dual";
	
	public static final String INSERTAR_EMPLEADO =  "insert into empleado values (null,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	public static final String SELECT_EMPLEADO_ID = "select * from empleado where id_empleado=?";
	
	public static final String SELECT_CORREO_EMPLEADO = "select * from empleado where email=?";
	
	public static final String SELECT_ID_TIPOID = "select * from empleado where tipo_id=? and numero_id=? and id_empleado != ?";
	
	public static final String ACTUALIZAR_EMPLEADO = "update empleado set  s_apellido = ?,p_nombre = ?,o_nombres = ? ,pais_empleo =? ,tipo_id = ?,numero_id = ?,email = ?,fecha_ingreso = ?,area = ?,estado = ?, fecha_hora_registro =?, p_apellido = ?,fecha_edicion = ? where id_empleado= ?";
	
}

