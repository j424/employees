package com.example.demo.Util;

public class Mensajes {

	public Mensajes() {
		
	}
	
	public static final String ERROR_PRIMER_NOMBRE = "el campo primer nombre no cumple con los estandares";
	public static final String ERROR_PRIMER_APELLIDO = "el campo primer apellido no cumple con los estandares";
	public static final String ERROR_SEGUNDO_NOMBRE = "el campo segundo nombre no cumple con los estandares";
	public static final String ERROR_SEGUNDO_APELLIDO = "el campo segundo apellido no cumple con los estandares";
	public static final String ERROR_OTROS_NOMBRES = "el campo otros nombres no cumple con los estandares";
	public static final String ERROR_NRO_ID = "el campo numero de identificacion no cumple con los estandares";
	public static final String DOBLE_ID = "el tipo y numero de identificacion ya se encuentra registrado";
	
	public static final String COLOMBIA = "Colombia";
	public static final String USA = "Estado Unidos";
	public static final String CC = "cedula cuidadania";
	public static final String CE = "cedula extranjera";
	public static final String PASAPORTE = "pasaporte";
	public static final String PERMISO = "permiso especial";
	public static final String ADMINISTRACION = "administracion";
	public static final String FINANCIERA = "financiera";
	public static final String COMPRAS = "compras";
	public static final String INFRAESTRUCTURA = "infraestructura";
	public static final String OPERACION = "operacion";
	public static final String TALENTO = "talento humano";
	public static final String SERVICIOS_VARIOS = "servicio varios";

	
	public static final String GUARDANDO = "guardando empleado en bd...";
	public static final String GUARDADO = "guardado con exito";
	public static final String BORRANDO = "borrando empleado con identificacion: ";
	public static final String BORRADO = "borrado con exito el empleado con identificacion: ";
	public static final String ACTUALIZANDO = "actualizando empleado con identificacion: ";
	public static final String ACTUALIZADO = "actualizado con exito el empleado con identificacion: ";
	
	
	public static final String ERROR = "ocurrio un error";

	
	
}
