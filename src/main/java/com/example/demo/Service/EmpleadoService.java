package com.example.demo.Service;




import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Entity.EmpleadoEntity;
import com.example.demo.Manager.EmpleadoManager;


// manejo de los metodos expuestos para los empleados


@RestController
public class EmpleadoService {

	@Autowired
	private EmpleadoManager empleadoManager;
	
	@RequestMapping(value = "/api/consultarTodoEmpleado")
	public List<EmpleadoEntity> consultaTodosEmpleados() throws SQLException {
		return empleadoManager.consultaTodosEmpleados();
	}
	
	@RequestMapping(value = "/api/registrarEmpleado", method = RequestMethod.POST)
	public void registrarEmpleado(@RequestBody EmpleadoEntity empleado) throws SQLException {
		 empleadoManager.registrarEmpleado(empleado);
	}
	
	@RequestMapping(value = "/api/eliminarEmpleado", method = RequestMethod.POST)
	public void eliminarEmpleado(@RequestBody Long idEmpleado) throws SQLException {
		 empleadoManager.eliminarEmpleado(idEmpleado);
	}
	
	@RequestMapping(value = "/api/consultaEmpleadoById", method = RequestMethod.POST)
	public EmpleadoEntity consultaEmpleadoById(@RequestBody Long idEmpleado) throws SQLException {
		 return empleadoManager.consultaEmpleadoById(idEmpleado);
	}
	
	@RequestMapping(value = "/api/actualizarEmpleado", method = RequestMethod.POST)
	public void actualizarEmpleado(@RequestBody EmpleadoEntity empleado) throws SQLException {
		 empleadoManager.actualizarEmpleado(empleado);
	}
	
}
