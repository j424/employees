package com.example.demo.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLEADO")
public class EmpleadoEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_EMPLEADO")
	private Long idEmpleado;
	
	@Column(name = "P_NOMBRE")
	private String pNombre;
	
	@Column(name = "O_NOMBRES")
	private String oNombres;
	
	@Column(name = "P_APELLIDO")
	private String pApellido;
	
	@Column(name = "S_APELLIDO")
	private String sApellido;
	
	@Column(name = "PAIS_EMPLEO")
	private String paisEmpleo;
	
	@Column(name = "TPO_ID")
	private String tipoId;
	
	@Column(name = "NUMERO_ID")
	private String numeroId;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "AREA")
	private String area;
	
	@Column(name = "FECHA_INGRESO")
	private String fechaIngreso;
	
	@Column(name = "ESTADO")
	private String estado;
	
	@Column(name = "FECHA_HORA_REGISTRO")
	private String fechaHoraRegistro;
	
	@Column(name = "FECHA_EDICION")
	private String fechaEdicion;

	public String getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(String fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

	public Long getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getpNombre() {
		return pNombre;
	}

	public void setpNombre(String pNombre) {
		this.pNombre = pNombre;
	}

	public String getoNombres() {
		return oNombres;
	}

	public void setoNombres(String oNombres) {
		this.oNombres = oNombres;
	}

	public String getpApellido() {
		return pApellido;
	}

	public void setpApellido(String pApellido) {
		this.pApellido = pApellido;
	}

	public String getsApellido() {
		return sApellido;
	}

	public void setsApellido(String sApellido) {
		this.sApellido = sApellido;
	}

	public String getPaisEmpleo() {
		return paisEmpleo;
	}

	public void setPaisEmpleo(String paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}

	public String getTipoId() {
		return tipoId;
	}

	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}

	public String getNumeroId() {
		return numeroId;
	}

	public void setNumeroId(String numeroId) {
		this.numeroId = numeroId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(String fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaHoraRegistro() {
		return fechaHoraRegistro;
	}

	public void setFechaHoraRegistro(String fechaHoraRegistro) {
		this.fechaHoraRegistro = fechaHoraRegistro;
	}

}
