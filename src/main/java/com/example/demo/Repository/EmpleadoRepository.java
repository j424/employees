package com.example.demo.Repository;

import java.sql.SQLException;
import java.util.List;

import com.example.demo.Entity.EmpleadoEntity;

public interface EmpleadoRepository {

	/**
	 * permite acceder a consultar todos los empleados de la bd
	 * @return
	 * @throws SQLException
	 */
	List<EmpleadoEntity> obtenerTodosEmpleados() throws SQLException;

	/**
	 * permite acceder al registro de un empleado
	 * @param empleado
	 * @return
	 * @throws SQLException
	 */
	EmpleadoEntity registrarEmpleado(EmpleadoEntity empleado) throws SQLException;;
	
	/**
	 * permite accedes a eliminar un empleado
	 * @param idEmpleado
	 * @throws SQLException
	 */
	void eliminarEmpleado(Long idEmpleado) throws SQLException;
	
	/**
	 * permite a acceder a consultar empleados por la id generada en bd
	 * @param idEmpleado
	 * @return
	 * @throws SQLException
	 */
	EmpleadoEntity consultaEmpleadoById(Long idEmpleado) throws SQLException;
	
	/**
	 * permite  consultar un correo en la bd
	 * @param correo
	 * @return
	 * @throws SQLException
	 */
	boolean existeCorreo(String correo)throws SQLException;
	
	/**
	 * permite actualizar un empleado 
	 * @param empleado
	 * @throws SQLException
	 */
	void actualizarEmpleado(EmpleadoEntity empleado)throws SQLException;;
	
	/**
	 * permite consultar si existe un numero y tipo de identificacion en la bd y que no tenga el mismo id generado
	 * @param tipoId
	 * @param numeroId
	 * @return
	 * @throws SQLException
	 */
	boolean existeTipoyNumeroId(String tipoId,String numeroId, Long idEmpleado)throws SQLException;;
	
}
