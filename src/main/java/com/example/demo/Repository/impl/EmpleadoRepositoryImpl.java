package com.example.demo.Repository.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.EmpleadoEntity;
import com.example.demo.Repository.EmpleadoRepository;
import com.example.demo.Util.Constantes;
import com.example.demo.Util.Consultas;
import com.example.demo.Util.Mensajes;

@Repository
public class EmpleadoRepositoryImpl implements EmpleadoRepository {

	@PersistenceContext()
	private EntityManager manager;

	private static Logger logJava = Logger.getLogger(EmpleadoRepositoryImpl.class);

	private Constantes constantes;
	private Mensajes mensajes;
	private Consultas consultas;

	@SuppressWarnings("static-access")
	@Override
	public List<EmpleadoEntity> obtenerTodosEmpleados() throws SQLException {

		List<EmpleadoEntity> lista = new ArrayList<>();
		Connection conn;

		conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

		Statement stmt = conn.createStatement();
		ResultSet rset = stmt.executeQuery(consultas.SELECT_TODO_EMPLEADO);
		while (rset.next()) {
			EmpleadoEntity em = new EmpleadoEntity();
			em.setpNombre(rset.getString("P_NOMBRE"));
			em.setoNombres(rset.getString("O_NOMBRES"));
			em.setpApellido(rset.getString("P_APELLIDO"));
			em.setsApellido(rset.getString("S_APELLIDO"));
			em.setArea(rset.getString("AREA"));
			em.setEmail(rset.getString("EMAIL"));
			em.setEstado(rset.getString("ESTADO"));
			em.setFechaEdicion(rset.getString("FECHA_EDICION"));
			em.setFechaHoraRegistro(rset.getString("FECHA_HORA_REGISTRO"));
			em.setFechaIngreso(rset.getString("FECHA_INGRESO"));
			em.setNumeroId(rset.getString("NUMERO_ID"));
			em.setPaisEmpleo(rset.getString("PAIS_EMPLEO"));
			em.setTipoId(rset.getString("TIPO_ID"));
			em.setIdEmpleado(Long.parseLong(rset.getString("ID_EMPLEADO")));
			lista.add(em);
		}

		stmt.close();

		return lista;

	}

	@SuppressWarnings("static-access")
	@Override
	public EmpleadoEntity registrarEmpleado(EmpleadoEntity empleado) throws SQLException {

		// Open a connection
		try {
			Connection conn;

			conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

			logJava.info(mensajes.GUARDANDO);

			String sql = consultas.INSERTAR_EMPLEADO;

			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, empleado.getsApellido());
			stmt.setString(2, empleado.getpNombre());
			stmt.setString(3, empleado.getoNombres());
			stmt.setString(4, empleado.getPaisEmpleo());
			stmt.setString(5, empleado.getTipoId());
			stmt.setString(6, empleado.getNumeroId());
			stmt.setString(7, empleado.getEmail());
			stmt.setString(8, empleado.getFechaIngreso());
			stmt.setString(9, empleado.getArea());
			stmt.setString(10, empleado.getEstado());
			stmt.setString(11, empleado.getFechaHoraRegistro());
			stmt.setString(12, empleado.getpApellido());
			stmt.setString(13, empleado.getFechaEdicion());

			// Execute a query
			stmt.executeQuery();

			String sql_currval = consultas.SELECT_SECUENCIA_EMPLEADO;
			Statement currvalStatement = conn.createStatement();
			ResultSet currvalResultSet = currvalStatement.executeQuery(sql_currval);

			if (currvalResultSet.next()) {
				empleado.setIdEmpleado(currvalResultSet.getLong(1));
			}

			logJava.info(mensajes.GUARDADO);
		} catch (SQLException e) {
			logJava.fatal(mensajes.ERROR + e, e);
		}

		return empleado;
	}

	@SuppressWarnings("static-access")
	@Override
	public void eliminarEmpleado(Long idEmpleado) throws SQLException {
		try {
			Connection conn;

			conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

			logJava.info(mensajes.BORRANDO + idEmpleado);

			String sql = consultas.DELETE_EMPLEADO;
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setLong(1, idEmpleado);

			// Execute a query
			stmt.executeQuery();
			logJava.info(mensajes.BORRADO + idEmpleado);
		} catch (SQLException e) {
			logJava.fatal(mensajes.ERROR + e, e);
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public EmpleadoEntity consultaEmpleadoById(Long idEmpleado) throws SQLException {

		EmpleadoEntity em = new EmpleadoEntity();
		Connection conn;
		conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

		String sql = consultas.SELECT_EMPLEADO_ID;
		PreparedStatement stmt = conn.prepareStatement(sql);

		stmt.setLong(1, idEmpleado);

		ResultSet rset = stmt.executeQuery();

		while (rset.next()) {

			em.setpNombre(rset.getString("P_NOMBRE"));
			em.setoNombres(rset.getString("O_NOMBRES"));
			em.setpApellido(rset.getString("P_APELLIDO"));
			em.setsApellido(rset.getString("S_APELLIDO"));
			em.setArea(rset.getString("AREA"));
			em.setEmail(rset.getString("EMAIL"));
			em.setEstado(rset.getString("ESTADO"));
			em.setFechaEdicion(rset.getString("FECHA_EDICION"));
			em.setFechaHoraRegistro(rset.getString("FECHA_HORA_REGISTRO"));
			em.setFechaIngreso(rset.getString("FECHA_INGRESO"));
			em.setNumeroId(rset.getString("NUMERO_ID"));
			em.setPaisEmpleo(rset.getString("PAIS_EMPLEO"));
			em.setTipoId(rset.getString("TIPO_ID"));
			em.setIdEmpleado(Long.parseLong(rset.getString("ID_EMPLEADO")));

		}

		stmt.close();

		return em;

	}

	@SuppressWarnings("static-access")
	@Override
	public boolean existeCorreo(String correo) throws SQLException {

		boolean hayCorreo = true;
		Connection conn;
		conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

		String sql = consultas.SELECT_CORREO_EMPLEADO;
		PreparedStatement stmt = conn.prepareStatement(sql);

		stmt.setString(1, correo);

		ResultSet rset = stmt.executeQuery();

		if (rset.next() == false) {
			hayCorreo = false;
		}

		return hayCorreo;
	}

	@SuppressWarnings("static-access")
	@Override
	public void actualizarEmpleado(EmpleadoEntity empleado) throws SQLException {
		try {
			Connection conn;

			conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);
			logJava.info(mensajes.ACTUALIZANDO + empleado.getIdEmpleado());

			String sql = Consultas.ACTUALIZAR_EMPLEADO;
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.setString(1, empleado.getsApellido());
			stmt.setString(2, empleado.getpNombre());
			stmt.setString(3, empleado.getoNombres());
			stmt.setString(4, empleado.getPaisEmpleo());
			stmt.setString(5, empleado.getTipoId());
			stmt.setString(6, empleado.getNumeroId());
			stmt.setString(7, empleado.getEmail());
			stmt.setString(8, empleado.getFechaIngreso());
			stmt.setString(9, empleado.getArea());
			stmt.setString(10, empleado.getEstado());
			stmt.setString(11, empleado.getFechaHoraRegistro());
			stmt.setString(12, empleado.getpApellido());
			stmt.setString(13, empleado.getFechaEdicion());
			stmt.setLong(14, empleado.getIdEmpleado());

			// Execute a query
			stmt.executeQuery();
			logJava.info(mensajes.ACTUALIZADO + empleado.getIdEmpleado());
		} catch (SQLException e) {
			logJava.fatal(mensajes.ERROR + e, e);
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean existeTipoyNumeroId(String tipoId, String numeroId, Long IdEmpleado) throws SQLException {
		boolean existeId = false;
		Connection conn;
		conn = DriverManager.getConnection(constantes.INFORMACION_CONEXION, constantes.USUARIO, constantes.CLAVE);

		String sql = consultas.SELECT_ID_TIPOID;
		PreparedStatement stmt = conn.prepareStatement(sql);

		stmt.setString(1, tipoId);
		stmt.setString(2, numeroId);
		stmt.setLong(3, IdEmpleado);
		
		ResultSet rset = stmt.executeQuery();

		if (rset.next() == true) {
			existeId = true;
		}

		return existeId;
	}

}
