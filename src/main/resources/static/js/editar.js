// carga de la pagina de edicion
$(document).ready(function() {
	let parametro = new URLSearchParams(location.search);
	var idEmpleado = parametro.get('idEmpleado');
   consultarEmpleado(idEmpleado);
      $('#sidebarToggle')[0].click();
});

// consumo de servicio para actuallizar edicion 

async function actualizarEmpleado() {

  let parametro = new URLSearchParams(location.search);
  var idEmpleado = parametro.get('idEmpleado');
  let empleado = {};
  empleado.pNombre = document.getElementById('txtNombre').value;
  empleado.oNombres = document.getElementById('txtNombre2').value;
  empleado.pApellido = document.getElementById('txtApellido').value;
  empleado.sApellido = document.getElementById('txtApellido2').value;
  empleado.paisEmpleo = document.getElementById('lstPais').value;
  empleado.tipoId = document.getElementById('lstTipoId').value;
  empleado.numeroId = document.getElementById('txtId').value;
  empleado.area = document.getElementById('lstArea').value;
  empleado.idEmpleado = idEmpleado;
  var fechaHoraReg =  sessionStorage.getItem('fechaHoraReg');
  
   var fechaIng = document.getElementById('fechaIng').value;
  
  var dia = fechaIng.substring(8,10);
  var mes = fechaIng.substring(5,7);
  var year = fechaIng.substring(0,4);

        
  var fechaIngFinal = dia + "/" + mes + "/" + year;
  empleado.fechaIngreso = fechaIngFinal;
  
  dia = fechaHoraReg.substring(8,10);
  mes =  fechaHoraReg.substring(5,7);
  year =  fechaHoraReg.substring(0,4);
   
    var fechaHoraRegistroFinal = dia + "/" + mes + "/" + year;
  empleado.fechaHoraRegistro = fechaHoraRegistroFinal;
  



  const request = await fetch('/api/actualizarEmpleado', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body:  JSON.stringify(empleado)
  })
  if(request.ok){
  alert("Proceso realizado!");
   window.location.href = 'index.html'

  }else{
	  alert("No se pudo actualizar!");
	}


}
  
// seteo de datos para carga de las listas y datos para la edicion

async function consultarEmpleado(idEmpleado) {


 const request = await fetch('/api/consultaEmpleadoById', {
    method: 'POST',
     headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body:  idEmpleado
  });
	
  const empleado = await request.json();
  
 
  
  document.querySelector('#txtNombre').value = empleado.pNombre;
  document.querySelector('#txtApellido').value = empleado.pApellido;
  document.querySelector('#txtNombre2').value = empleado.oNombres;
  document.querySelector('#txtApellido2').value = empleado.sApellido;
  document.querySelector('#txtId').value = empleado.numeroId;
  
  sessionStorage['fechaHoraReg']= empleado.fechaHoraRegistro
  
  document.querySelector('#fechaIng').value = empleado.fechaIngreso;
   
  
  if(document.querySelector('#lstArea').value == "administracion"){
	document.querySelector('#lstArea').value = 1;
}else if(document.querySelector('#lstArea').value == "administracion"){
	document.querySelector('#lstArea').value = 2;
}else if(document.querySelector('#lstArea').value == "financiera"){
	document.querySelector('#lstArea').value = 3;
}else if(document.querySelector('#lstArea').value == "compras"){
	document.querySelector('#lstArea').value = 4;
}else if(document.querySelector('#lstArea').value == "infraestructura"){
	document.querySelector('#lstArea').value = 5;
}else if(document.querySelector('#lstArea').value == "talento humano"){
	document.querySelector('#lstArea').value = 6;
}else if(document.querySelector('#lstArea').value == "servicio varios"){
	document.querySelector('#lstArea').value = 7;
}
  

if(document.querySelector('#lstPais').value == "colombia"){
	document.querySelector('#lstPais').value = 1;
}else if(document.querySelector('#lstPais').value == "usa"){
	document.querySelector('#lstPais').value = 2;
}



if(document.querySelector('#lstTipoId').value== "cedula cuidadania"){
	document.querySelector('#lstTipoId').value = 1;
}else if(document.querySelector('#lstTipoId').value == "cedula extranjera"){
	document.querySelector('#lstTipoId').value = 2;
}else if(document.querySelector('#lstTipoId').value == "pasaporte"){
	document.querySelector('#lstTipoId').value = 3;
}else if(document.querySelector('#lstTipoId').value == "permiso especial"){
	document.querySelector('#lstTipoId').value = 4;
}



}

