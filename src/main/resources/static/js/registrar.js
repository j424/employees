// carga de la pagina registro

$(document).ready(function() {
   // on ready
    $('#sidebarToggle')[0].click();
   validacionFechaIngreso();
});

// validacion de fecha ingreso

function validacionFechaIngreso(){
	
var today = new Date();

var dd = today.getDate();
var mm = today.getMonth() + 1; //January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
   dd = '0' + dd;
}

if (mm < 10) {
   mm = '0' + mm;
}    
  
today = dd  + '-' + mm + '-' + yyyy;

document.getElementById("fechaIng").setAttribute("max", today);

}

// consumo de servicio para registrar un empleado

async function registrarEmpleado() {
  let empleado = {};
    empleado.idEmpleado = 0;
  empleado.pNombre = document.getElementById('txtNombre').value;
  empleado.oNombres = document.getElementById('txtNombre2').value;
  empleado.pApellido = document.getElementById('txtApellido').value;
  empleado.sApellido = document.getElementById('txtApellido2').value;
  empleado.paisEmpleo = document.getElementById('lstPais').value;
  empleado.tipoId = document.getElementById('lstTipoId').value;
  empleado.numeroId = document.getElementById('txtId').value;
  empleado.area = document.getElementById('lstArea').value;
  
  var fechaIng = document.getElementById('fechaIng').value;
  
  var dia = fechaIng.substring(8,10);
  var mes = fechaIng.substring(5,7);
  var year = fechaIng.substring(0,4);

        
        var fechaIngFinal = dia + "/" + mes + "/" + year;


 empleado.fechaIngreso = fechaIngFinal;

  const request = await fetch('/api/registrarEmpleado', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body:  JSON.stringify(empleado)
  });
 if(request.ok){
  alert("Proceso realizado!");
   window.location.href = 'index.html'

  }else{
	  alert("No se pudo guardar!");
	}
}
