// carga de pagina
$(document).ready(function() {
 cargarEmpleados();
    $('#tEmpleado').DataTable();
    $('#sidebarToggle')[0].click();
} );


  let listadoHtml = '';

// consumo de servicio para llenar la tabla principal

  async function cargarEmpleados() {
  const request = await fetch('/api/consultarTodoEmpleado', {
    method: 'GET',
    headers: getHeaders()
  });
  
  const empleados = await request.json();

for (let empleado of empleados){
	 let botonEliminar = '<a href="#" onclick="eliminarEmpleado(' + empleado.idEmpleado + ')" class="btn btn-danger btn-circle btn-sm" title="eliminar"><i class="fas fa-trash"></i></a>';
	 
	let botonEditar = "<button onclick='editarEmpleado("+empleado.idEmpleado+")'  class='btn btn-primary btn-circle btn-sm' title='Editar'><i class='fas fa-pen'></i></button>";


let usuarioHtml = '<tr><td>'+empleado.pNombre+'</td><td>' + empleado.oNombres + '</td><td>' + empleado.pApellido + '</td><td>'
                    + empleado.sApellido  + '</td><td>'+ empleado.tipoId + '</td><td>' + empleado.numeroId +'</td><td>' 
					 + empleado.paisEmpleo + '</td><td>' + empleado.email + '</td><td>' + empleado.estado + '</td><td>'
                    + botonEliminar + '' + '</td><td>' + botonEditar   +'</td></tr>';
    listadoHtml += usuarioHtml;
	}
document.querySelector('#tEmpleado tbody').outerHTML = listadoHtml;
}


function getHeaders() {
    return {
     'Accept': 'application/json',
     'Content-Type': 'application/json'
   };
}

// consumo de servicio para eliminar un empleado

async function eliminarEmpleado(idEmpleado) {

  if (!confirm('¿Desea eliminar este empleado? ')) {
    return;
  }

 const request = await fetch('/api/eliminarEmpleado', {
    method: 'POST',
    headers: getHeaders(),
    body:  idEmpleado
  });
	
  location.reload()
}

function editarEmpleado(idEmpleado){
window.location.href = "edicion.html?idEmpleado=" + idEmpleado;

}
